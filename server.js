const http = require('http');
const request = require('request');
const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const translate = require('google-translate-api');
const ISO6391 = require('iso-639-1');
const Speech = require('@google-cloud/speech');
//const bodyParser = require('body-parser');
const formidable = require('formidable');
const fs = require('fs');

// Instantiates a client
const speechClient = Speech({
    projectId: 'speech-hackathon-shared',
});

const app = express();
// parse POST parameters into JSON
// app.use(bodyParser.json({limit: '50mb', parameterLimit: 1000000}));
// app.use(bodyParser.raw({ type: 'audio/wav', limit: '50mb' }));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));
const router = express.Router();

const indexController = require('./server/controllers/indexController');


//app.use(express.static('.app/views/'));

const appDir = path.join(__dirname, '/app');
console.log(appDir);

const appPaths = {
    root: path.resolve(appDir),
    views: path.resolve(appDir, 'views'),
    layout: path.resolve(appDir, 'views/layouts'),
    js: path.resolve(appDir, 'js'),
    css: path.resolve(appDir, 'css'),
    asset: path.resolve(appDir, 'asset')

};

// setup options for serving static content
const staticOptions = {
    etag: true,
    lastModified: true,
    maxAge: 0
};

const hbs = exphbs.create({
    layoutsDir: appPaths.layout,
    extname: '.hbs',
    defaultLayout: 'main'
});

app.set('view engine', 'hbs');
app.set('views', appPaths.views);
app.engine('.hbs', hbs.engine);

app.use('/', indexController(router, request, translate, ISO6391, speechClient, formidable, fs, path));
app.use('/js', express.static(appPaths.js, staticOptions));
app.use('/css', express.static(appPaths.css, staticOptions));
app.use('/asset', express.static(appPaths.asset, staticOptions));


console.log('Server started');

app.listen(8080);