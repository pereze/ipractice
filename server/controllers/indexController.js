module.exports = (router,request, translate, ISO6391, speechClient, formidable, fs, path ) => {

         var correctText = "";
        router.get('/', function(req, res){
          res.render('index');
        });

        router.get("/translate", function(req, res) {
          var textToTranslate = req.query.text;

          var destLanguage = req.query.destLang;

          res.render('index', {title: 'translate'}, function(err, html) {
             var destLangCode = ISO6391.getCode(destLanguage);

             // work around since zh-CN is google translate uses for Chinese.
             if (destLangCode == "zh") {
                destLangCode = "zh-CN";
             }

             console.log("get destLang:" + destLanguage + ", code:" + destLangCode);

             translate(textToTranslate,{to: destLangCode}).then(resp=>{
                this.correctText = resp.text;
                res.send(resp.text);
              }).catch(err=>{
                console.log(err);
              })
          });

        });

        router.get("/speechform", function (req, res) {
            res.send('<form action="/speech" enctype="multipart/form-data" method="post">' +
                '<input type="file" name="audio" /><button>send</button>' +
                '</form>')
        })

        router.post("/speech", function(req, res){
            var form = new formidable.IncomingForm();
            form.uploadDir = path.join(__dirname, '/uploads');
            var filepath = "";
            form.on('file', function(field, file){
               //fs.rename(file.path, path.join(form.uploadDir, file.name), function (err) { console.log("Error renaming file", err); });
                filepath = file.path;
            });
            form.on('error', function(error){
                console.log("ERROR when uploading a file: ", error);
            })
            form.on('end', function(){
                //console.log("file uploaded with name", filename);
                //const filepath = path.join(form.uploadDir, filename);
                console.log("filepath is ", filepath);
                fs.readFile(filepath, function(err, data) {
                    console.log("reading the file");
                    const audio = {content: data};
                    const config = {
                        encoding: 'LINEAR16',
                        sampleRateHertz: 44100,
                        languageCode: req.query.lang
                    };

                    const request = {
                        audio: audio,
                        config: config
                    };

                    console.log("calling the api with the request", request);
                    speechClient.recognize(request).then((results) => {
                        const transcription = results[0].results[0].alternatives[0].transcript;
                        const confidence = results[0].results[0].alternatives[0].confidence;
                        console.log(`Transcription: ${transcription}`);
                        console.log(`Full results: ${results}`);
                        res.send(transcription +","+ this.correctText + "," + confidence);

                    }).catch((err) => {
                        console.error('ERROR:', err);
                         res.send("ERROR");
                    }).then(() => {
                        fs.unlinkSync(filepath);
                    });
                });
            })
            form.parse(req);
        });
    
        return router;
    };