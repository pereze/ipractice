
This is a NodeJS project called iPractice which aims to help user learn a foreign language.

How to run this NodeJS application:

 1. open a terminal: type: export GOOGLE_APPLICATION_CREDENTIALS=./config/keyfile.json

 2. in the terminal: type: npm install

 3. in the terminal: type: npm start
 
 4. open a internet browser(e.g. google chrome): type: http://localhost:8080/

 You should see the UI now and start practising...
