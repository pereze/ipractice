iPractice.recorder = null;

iPractice.sendData = (data, lang) => {
    form = new FormData(),
    form.append("file",data);

    request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if (request.readyState == 4 && request.status == 200)
        {
            console.log("result", request.responseText);
            iPractice.result(request.responseText); // Another callback here
        }
    }
    console.log("in sendData of mic.js. destLang:" + iPractice.destLang);
    var langCodeForGoogleAPI = iPractice.destLang;

    //work around for Chinese, since the API not use IOS386-1 code.
    if (langCodeForGoogleAPI == "zh") {
      langCodeForGoogleAPI = "cmn-Hans-CN";
    }

    request.open(
        "POST",
        "/speech?lang=" + iPractice.destLang,
        true
    );
    request.send(form);
};

iPractice.captureMic = (callback) => {
    navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
    navigator.getUserMedia({audio: true}, callback, function(error) {
        alert('Unable to access your microphone.');
        console.error(error);
    });
};

iPractice.stopRecordingCallback = () => {
    var blob = iPractice.recorder.getBlob();
    iPractice.recorder.microphone.stop();
    console.log("jessie log in mic in stopRecording. lang:"+ iPractice.recorderlang)
    iPractice.sendData(blob, iPractice.recorderlang);
};

iPractice.startRecording = (langCode) => {
    iPractice.recorder = null;
    iPractice.recorderlang = langCode;
    iPractice.captureMic(function(microphone) {
        iPractice.recorder = RecordRTC(microphone, {
            type: 'audio',
            recorderType: StereoAudioRecorder,
            numberOfAudioChannels: 1,
            desiredSampRate: 44100
        });

        iPractice.recorder.startRecording();
        // release microphone on stopRecording
        iPractice.recorder.microphone = microphone;

         //
         setTimeout(() => {
             iPractice.recorder.stopRecording(iPractice.stopRecordingCallback);
         }, 3000);
    });
};

iPractice.result = (data) => {

  console.log("results", data);
  if (data.includes("ERROR")) {
      iPractice.play("Sorry I didn't catch that. Please say the word now. If you want to hear the word again. Say repeat.");
  } else {
      const t = data.split(",");
      console.log("your voice text:" + t[0]);
      console.log("correct text:" + t[1]);
      console.log("confidence rate:" + t[2]);
      iPractice.getFeedback(t[0], t[1], t[2]);
  }
};