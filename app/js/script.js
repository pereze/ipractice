var iPractice = {
};

iPractice.inputWebkit = null;
iPractice.inputWebkitStarted = false;
iPractice.outputWebkit = null;
iPractice.outputWebkitStopped = false;
iPractice.destLang = "en";
iPractice.correctText = "";

iPractice.correctText = "";

var SW = new SiriWave({
		style: 'ios9',
		speed: 0.00,
		amplitude: 1,
		container: document.getElementById('spectrum'),
		autostart: true,
	});

iPractice.listen = () => {
    if (iPractice.inputWebkit == null ) {
        iPractice.inputWebkit= new webkitSpeechRecognition();
        iPractice.inputWebkit.continuous = false;
        iPractice.inputWebkit.interimResults = false;
    }

    iPractice.inputWebkit.lang = "en-US";
    if (!iPractice.inputWebkitStarted) {
        iPractice.inputWebkit.start();
        iPractice.inputWebkitStarted = true;
        changeImage(true);
    } else {
        iPractice.inputWebkitStarted = false;
        iPractice.inputWebkit.stop();
        changeImage(false);

    }

    iPractice.inputWebkit.onresult = (e) => {
        //capture the word from user
       const originalText = e.results[0][0].transcript;
       console.log("User voice input: " + originalText);
       changeImage();

        if (iPractice.inputWebkitStarted) {
               iPractice.inputWebkit.stop();
               iPractice.inputWebkitStarted = false;
               changeImage(false);
        }

       const capturedText = iPractice.getCaptureText(originalText, " say ", " in ");
       const languageName = iPractice.getLanguageName(originalText, " in ");

       // translate it
      iPractice.getTranslation(languageName, capturedText, mycallback);
    }

    iPractice.inputWebkit.onerror = (err) => {
        changeImage(false);
        console.log(err);
        recognition.stop();
    };


};

iPractice.getCaptureText = (originalText, identifyWord, languageIdentifyWord) => {
    const startIndex = originalText.indexOf(identifyWord) + identifyWord.length;
    const endIndex = originalText.lastIndexOf(languageIdentifyWord);
    return originalText.slice(startIndex,endIndex);
}

iPractice.getLanguageName = (originalText) =>  {
    var n = originalText.split(" ");
    return n[n.length - 1];
}

iPractice.getTranslation = (languageName, text, callback) => {
    const request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            console.log("language name:" + languageName );
            const languageCode = iPractice.getLanguageCode(languageName);
            console.log("language code:" + languageCode);
            callback(request.responseText, languageCode);
        }
    };
    console.log("before translate. text is: " + text + ", language is: " + languageName );

    const url = "/translate?destLang=" + languageName + "&&text=" + text;
    request.open('GET', url);
    request.send();
}

function mycallback(convertedText, langCode) {
    iPractice.correctText = convertedText;
    console.log("play: " + convertedText + " using: "+ langCode );
    iPractice.play(convertedText, langCode);
    iPractice.destLang = langCode;

    console.log("in mycallback. set destLang:" + iPractice.destLang);

    //pause for 3 seconds.
    setTimeout(() => {
     console.log("timeout in 5 seconds.")
      if (iPractice.outputWebkitStopped) {
          changeImage(false);
          iPractice.practise();
      }
    }, 3000);
}

iPractice.play = (text, lang) => {
    if (iPractice.outputWebkit == null ) {
        iPractice.outputWebkit = new SpeechSynthesisUtterance();
        var voices = window.speechSynthesis.getVoices();
        window.speechSynthesis.voice = voices.filter(function(voice) { return voice.name == 'Google UK English Male'; })[0];
    }

    iPractice.outputWebkit.text = text;
    if (lang) {
        iPractice.outputWebkit.lang = lang;
    } else {
        iPractice.outputWebkit.lang = "en";
    }

    window.speechSynthesis.speak(iPractice.outputWebkit);

        iPractice.outputWebkit.onstart = function (event) {
             iPractice.outputWebkitStopped = false;
             changeImage(true);
        };

    iPractice.outputWebkit.onend = function (event) {
         //console.log("onend play callback: text:" + text);
             changeImage(false);

         if (text == "Do you want to practise now") {
             iPractice.inputWebkit.start();
             changeImage(true);
             iPractice.inputWebkit.onresult = (e) => {
                     //capture the word from user
                    changeImage()
                    const originalText = e.results[0][0].transcript;

                    if (originalText.includes("yes")) {
                        iPractice.inputWebkit.stop();
                        iPractice.inputWebkitStarted = false;
                        console.log("user said yes");
                        iPractice.play("Try to say the word now.");
                        iPractice.userReplyYes = true;

                    } else if (originalText.includes("no")) {
                        // iPractice.play("You said no.");
                        console.log("user said no");
                        iPractice.userReplyNo = true;
                        iPractice.play("Ok, not this time. Goodbye.");
                    } else {
                       iPractice.play("Sorry I don't understand your answer. Do you want to practise now?");
                       console.log("user wrong input")

                    }
             }
         } else if (text == "Try to say the word now.") {
             iPractice.startRecording();

         } else if (text == "Sorry I don't understand your answer. Do you want to practise now?") {
            // give user a second chance to practise saying the foreign language.
                iPractice.inputWebkit.start();
                iPractice.inputWebkit.onresult = (e) => {
                     //capture the word from user
                    const originalText = e.results[0][0].transcript;

                    if (originalText == "yes") {
                        iPractice.inputWebkit.stop();
                        iPractice.inputWebkitStarted = false;
                        console.log("user said yes");
                        iPractice.play("Try to say the word now.");
                        iPractice.userReplyYes = true;

                    } else if (originalText == "no") {
                        // iPractice.play("You said no.");
                        console.log("user said no");
                        iPractice.userReplyNo = true;
                        iPractice.play("Ok, not this time. Goodbye.");
                    } else {
                       iPractice.play("Sorry I don't understand your answer. Goodbye!");
                       console.log("user wrong input for the second time.")
                    }
                }
         } else if (text.includes ("repeat")) {
               iPractice.inputWebkit.start();
               iPractice.inputWebkit.onresult = (e) => {
                    //capture the word from user
                   const originalText = e.results[0][0].transcript;

                   if (originalText == "repeat") {
                       iPractice.inputWebkit.stop();
                       iPractice.inputWebkitStarted = false;
                       console.log("user said repeat: CORRECT TEXT IS:" + iPractice.correctText);
                       iPractice.play(iPractice.correctText, iPractice.destLang);
                   }
               }
         }

         iPractice.outputWebkitStopped = true;
    };
}

iPractice.getLanguageCode = (name) => {
    for(var i = 0; i < langCodes.length; i++) {
        var obj = langCodes[i];

        if (obj.name.includes(name)) {
          console.log("getLanguageCode:" + obj.code);
         return obj.code;
        }
    }

    console.log("not found language code. use default english as code.");
    return "en";
}

function changeImage(record) {
    if (record == true) {
        document.getElementsByClassName('fa-microphone')[0].style.color = "red";
        document.getElementById('startCss').innerHTML = "STOP";
        SW.setAmplitude(1);
        SW.setSpeed(0.05);
    } else {
        document.getElementsByClassName('fa-microphone')[0].style.color= "white";
        document.getElementById('startCss').innerHTML = "START";
        SW.setAmplitude(0);
        iPractice.inputWebkit.stop();
    }


}

