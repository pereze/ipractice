 iPractice.getFeedback = (userVoice, originalTranslation, confidenceLevel) => {
    
    if (userVoice.toLowerCase() == originalTranslation.toLowerCase()) {
          
        if (confidenceLevel > 0.95) {
            return iPractice.play("Excellent. You sound like a native speaker!");
        }
        else if (confidenceLevel >= 0.8) {
            
            return iPractice.play("Almost there, keep going!");
    
        } else if (confidenceLevel >= 0.5) {
    
            return iPractice.play("Almost, give it another go");
    
        } else if (confidenceLevel >= 0.3) {
            
            return iPractice.play("Not quite, try again");
        }
        
    } else {
        
         return iPractice.play("You pronounced the wrong word. Keep practicing");
    }

}